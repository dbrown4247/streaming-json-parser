package json

import (
	"io"
	"log"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

type Decoder struct {
	d *AsyncByteReader
}

type AsyncByteReader struct {
	r          io.Reader
	bufferChan chan []byte
	buffer     []byte
	done       bool
}

func NewAsyncByteReader(r io.Reader) *AsyncByteReader {
	return &AsyncByteReader{
		r:          r,
		bufferChan: make(chan []byte, 10),
		buffer:     make([]byte, 100),
	}
}

func (e *AsyncByteReader) MaintainBuffer() {
	for {
		tmp := make([]byte, 100)
		_, err := e.r.Read(tmp)
		if err != nil {
			log.Println(err)
			e.done = true
			return
		}
		e.bufferChan <- tmp
	}
}

func (e *AsyncByteReader) Get() []byte {
	return <-e.bufferChan
}

// NewJsonDecoder returns a new instance of a decoder object
func NewJsonDecoder(r io.Reader) *Decoder {
	return &Decoder{
		d: NewAsyncByteReader(r),
	}
}

func (e *Decoder) Decode(output interface{}) {
	go e.d.MaintainBuffer()
	for !e.d.done {
		e.getDecodeInterface(output)
	}
}

func (e *Decoder) getDecodeInterface(o interface{}) {
	var key []byte
	var literal []byte
OuterLoop:
	for !e.d.done || len(e.d.buffer) > 0 {
		for i, v := range e.d.buffer {
			switch v {
			case '{':
				if len(key) != 0 {
					e.d.buffer = e.d.buffer[i+1:]
					e.getDecodeInterface(o.(map[string]interface{})[string(key)])
					goto OuterLoop
				}
			case '[':
				e.d.buffer = e.d.buffer[i+1:]
				for {
					e.getDecodeInterface(o.(map[string]interface{})[string(key)])
					if len(key) == 0 {
						o = append(o.([]interface{}), tmp)
					} else {
						o = append(o.(map[string]interface{})[string(key)].([]interface{}), tmp)
					}
				}
			case '}':
				e.d.buffer = e.d.buffer[i+1:]
				return
			case '"':
				e.d.buffer = e.d.buffer[i+1:]
				if len(key) == 0 {
					key, _ = e.readUntil(readUntilDoubleQuote)
					log.Println(string(key))
				} else {
					literal, _ = e.readUntil(readUntilDoubleQuote)
					log.Print(string(literal))
				}
				goto OuterLoop
			case '-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
				e.d.buffer = e.d.buffer[i+1:]
				literal, _ = e.readUntil(readUntilLiteral)
				log.Print(string(literal))
				goto OuterLoop
			}
		}
		e.d.buffer = e.d.Get()
	}
}

func (e *Decoder) readUntil(until map[byte]interface{}) ([]byte, byte) {
	var segment []byte
	for {
		for i, v := range e.d.buffer {
			if _, ok := until[v]; ok {
				segment = append(segment, e.d.buffer[:i]...)
				e.d.buffer = e.d.buffer[i+1:]
				return segment, v
			}
		}
		segment = append(segment, e.d.buffer...)
		e.d.buffer = e.d.Get()
	}
}

var readUntilDoubleQuote = map[byte]interface{}{
	'"': struct{}{},
}
var readUntilLiteral = map[byte]interface{}{
	',': struct{}{},
	']': struct{}{},
	'}': struct{}{},
}

var iOffsetMap = map[byte]int{
	'f':  4,
	't':  3,
	'\\': 1,
}
