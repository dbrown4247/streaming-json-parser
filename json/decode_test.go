package json

import (
	"log"
	"os"
	"testing"
)

func TestNewDecoder(t *testing.T) {
	f, err := os.Open("C:\\Users\\Dorkinator\\go\\src\\streaming-json\\assets\\scratch.json")
	if err != nil {
		log.Println(err)
		return
	}
	NewJsonDecoder(f).Decode("")
}
