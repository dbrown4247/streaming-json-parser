package json

type op int

const (
	UnknownOP = iota
	Literal
	KeyStart
	KeyEnd
	StringStart
	StringEnd
	ArrayStart
	ArrayEnd
	StructStart
	StructEnd
)

var opString = map[op]string{
	UnknownOP:   "UnknownOP",
	Literal:     "Literal",
	KeyStart:    "KeyStart",
	KeyEnd:      "KeyEnd",
	StringStart: "StringStart",
	StringEnd:   "StringEnd",
	ArrayStart:  "ArrayStart",
	ArrayEnd:    "ArrayEnd",
	StructStart: "StructStart",
	StructEnd:   "StructEnd",
}

var nextOp = map[byte]func(op op) op{
	'"': func(op op) op {
		switch op {
		case StringStart:
			return StringEnd
		case KeyStart:
			return KeyEnd
		case StructStart, StructEnd:
			return KeyStart
		default:
			return StringStart
		}
	},
	'{': getOpFunc(StructStart),
	'}': getOpFunc(StructEnd),
	'[': getOpFunc(ArrayStart),
	']': getOpFunc(ArrayEnd),
}

func getOpFunc(out op) func(op op) op {
	return func(op op) op {
		return out
	}
}

func getNextOp(in byte, cur op) op {
	if f, ok := nextOp[in]; ok {
		return f(cur)
	}
	return UnknownOP
}

func (e *op) String() string {
	return opString[*e]
}
